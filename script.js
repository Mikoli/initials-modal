"use strict";

/////////////////////////////////////////////////////////////////

//Funkcija koja vraća inicijale

document.querySelector(".check").addEventListener("click", function () {
    let imePrezime = document.querySelector(".imeIPrezime").value;
    let vasiInicijaliSu;

    for (let i = 0; i < imePrezime.length; i++) {
        if (imePrezime[i] === " ") {
            vasiInicijaliSu = imePrezime[0] + "." + imePrezime[++i] + ".";
        }
    }
    document.querySelector(".inicijali").textContent = JSON.stringify(vasiInicijaliSu);
});

////////////////////////////////////////

//Refreshing Input placeholder="Unesite ime i prezime"

function refresh() {
    document.getElementById('imeIPrezime').value = "Unesite ime i prezime";
}

/////////////////////////////////////////////////////////////////

//Refreshing cele web strane, kao i Input placeholder="Unesite ime i prezime"

/* function refresh() {
    document.querySelector(".imeIPrezime").value = window.location.reload();
} */

//OR

/* function refresh() {    
    setTimeout(function () {
        location.reload()
    }, 100);
} */